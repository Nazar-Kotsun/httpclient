using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using CoolParking.BL.Models;

namespace CoolParkingClient
{
    public class HttpClientMenu
    {
        private bool Exit = false;
        private int Choose;
 
        private HttpClient client;

        public HttpClientMenu()
        {
            HttpClientHandler httpClientHandler = new HttpClientHandler();
            httpClientHandler.ServerCertificateCustomValidationCallback = (httpRequestMessage, cert, cetChain, policyErrors) => true;
            
            client = new HttpClient(httpClientHandler);
            client.BaseAddress = new Uri("https://localhost:5001");
        }
        
        public void StartMenu()
        {
            while (!Exit)
            {
                ShowMenu();
            }
        }
        
        private void ShowStyleLine(string message)
        {
            Console.WriteLine("-0-*-0-*-0-*-0-*-0-*-" + message + "-*-0-*-0-*-0-*-0-*-0-");
        }
        
        private void StopProgramToContinue()
        {
            Console.Write("Please enter any key to continue...");
            Console.ReadKey();
            Console.Clear();
        }
        
        private void CheckCorrectEnteringInteger(out int value)
        {
            while (!int.TryParse(Console.ReadLine(), out value))
            {
                Console.WriteLine("Incorrect entering!");
            }
            
        }
        private void ShowMenu()
        {
            Console.WriteLine("1 - Show current balance of parking");
            Console.WriteLine("2 - Show sum of money for the current period");
            Console.WriteLine("3 - Show count of free places");
            Console.WriteLine("4 - Show all the transactions of parking for the current period");
            Console.WriteLine("5 - Show history of transactions");
            Console.WriteLine("6 - Show list of vehicles");
            Console.WriteLine("7 - Put the vehicle on the parking");
            Console.WriteLine("8 - Pick up vehicle at the parking");
            Console.WriteLine("9 - Replenish the balance of a particular car");
            Console.WriteLine("0 - Exit");
            DoChoice();
        }
        
        private void DoChoice()
        {
            Console.Write("Your choice: ");
            CheckCorrectEnteringInteger(out Choose);

            switch (Choose)
            {
                case 1:
                {
                    ShowCurrentBalanceOfParking();
                    StopProgramToContinue();
                    break;
                }
                case 2:
                {
                    ShowSumOfMoneyForTheCurrentPeriod();
                    StopProgramToContinue();
                    break;
                }
                case 3:
                {
                    ShowCountOnFreePlaces();
                    StopProgramToContinue();
                    break;
                }
                case 4:
                {
                    ShowLastParkingTransactions();
                    StopProgramToContinue();
                    break;
                }
                case 5:
                {
                    ShowAllTheTransactions();
                    StopProgramToContinue();
                    break;
                }
                case 6:
                {
                    ShowListOfVehicles();
                    StopProgramToContinue();
                    break;
                }
                case 7:
                {
                    AddVehicle();
                    StopProgramToContinue();
                    break;
                }
                case 8:
                {
                    RemoveVehicle();
                    StopProgramToContinue();
                    break;
                }
                case 9:
                {
                    ReplenishTheBalance();
                    StopProgramToContinue();
                    break;
                }
                case 0:
                {
                    ExitTheProgram();
                    break;
                }
                default: ShowCurrentBalanceOfParking(); break;
            }
            
        }

        private void ShowCurrentBalanceOfParking()
        {
          
            var respone = client.GetAsync("/api/parking/balance").Result;
            ShowStyleLine("Respone - " + respone.StatusCode.ToString());
            
            if (respone.IsSuccessStatusCode)
            {
                ShowStyleLine("Current balance");
                Console.WriteLine(respone.Content.ReadAsStringAsync().Result);
            }
        }

        private void ShowSumOfMoneyForTheCurrentPeriod()
        {
            
            var respone = client.GetAsync("/api/transactions/last").Result;
            ShowStyleLine("Respone - " + respone.StatusCode);
            
            if (respone.IsSuccessStatusCode)
            {
                ShowStyleLine("Sum of money for the current period");
                IEnumerable<TransactionInfo> transactions =
                    JsonSerializer.Deserialize<IEnumerable<TransactionInfo>>(respone.Content.ReadAsStringAsync().Result);
                decimal sum = 0;
                foreach (var transaction in transactions)
                {
                    sum += transaction.Sum;
                }

                Console.WriteLine("Sum: " + sum);
            }
        }
        private void ShowCountOnFreePlaces()
        {
            
            var respone = client.GetAsync("/api/parking/freePlaces").Result;
            ShowStyleLine("Respone - " + respone.StatusCode.ToString());
            
            if (respone.IsSuccessStatusCode)
            {
                ShowStyleLine("Count of free places");
                Console.WriteLine("Count: " + respone.Content.ReadAsStringAsync().Result);
            }
           
        }

        private void ShowLastParkingTransactions()
        {
            var respone = client.GetAsync("/api/transactions/last").Result;
            ShowStyleLine("Respone - " + respone.StatusCode);
            
            if (respone.IsSuccessStatusCode)
            {
                ShowStyleLine("Last transactions");
                IEnumerable<TransactionInfo> transactions =
                    JsonSerializer.Deserialize<IEnumerable<TransactionInfo>>(respone.Content.ReadAsStringAsync()
                        .Result);
                
                foreach (var transaction in transactions)
                {
                    Console.WriteLine(transaction);
                }
            }
        }

        private void ShowAllTheTransactions()
        {
            var respone = client.GetAsync("/api/transactions/all").Result;
            ShowStyleLine("Respone - " + respone.StatusCode);
            
            Console.WriteLine(respone.Content.ReadAsStringAsync().Result);
            
        }

        private void ShowListOfVehicles()
        {
            var respone = client.GetAsync("/api/vehicles").Result;
            ShowStyleLine("Respone - " + respone.StatusCode);
            
            if (respone.IsSuccessStatusCode)
            {
                ShowStyleLine("Vehicles");
                
                IReadOnlyCollection<VehicleService> onlyCollection = 
                    JsonSerializer.Deserialize<IReadOnlyCollection<VehicleService>>(respone.Content.ReadAsStringAsync().Result);
                Console.WriteLine(onlyCollection.Count);
                
                foreach (var i in onlyCollection)
                {
                    Console.WriteLine(i);
                }
            }
            
            ShowStyleLine("");
        }

        private void AddVehicle()
        {
            ShowStyleLine("Add new vehicle");
            VehicleService vehicleService = new VehicleService();

            Console.Write("Enter new id for vehicle: ");
            vehicleService.Id = Console.ReadLine();

            
            Console.WriteLine("Choose type vehicle:");
            Console.WriteLine("1 - PassengerCar");
            Console.WriteLine("2 - Truck");
            Console.WriteLine("3 - Bus");
            Console.WriteLine("4 - Motorcycle");
            
            int chooseType;
            ShowStyleLine("Type");
            do
            {
                Console.Write("Enter type: ");
                
            } while (!int.TryParse(Console.ReadLine(), out chooseType));

            switch (chooseType)
            {
                case 1:
                {
                    vehicleService.VehicleType = VehicleType.PassengerCar;
                    break;
                }
                case 2:
                {
                    vehicleService.VehicleType = VehicleType.Truck;
                    break;
                }
                case 3:
                {
                    vehicleService.VehicleType = VehicleType.Bus;
                    break;
                }
                case 4:
                {
                    vehicleService.VehicleType = VehicleType.Motorcycle;
                    break;
                }
                default: vehicleService.VehicleType = VehicleType.PassengerCar; break;
            }
            
            ShowStyleLine("Balance");
            decimal balance;
            do
            {
                Console.Write("Enter sum of money: ");
                
            } while (!decimal.TryParse(Console.ReadLine(), out balance));

            vehicleService.Balance = balance;

            string strSerializeVehicle = JsonSerializer.Serialize<VehicleService>(vehicleService);
            
            HttpContent httpContent = new StringContent(strSerializeVehicle, Encoding.UTF8, "application/json");
            var respone = client.PostAsync("/api/vehicles", httpContent).Result;
            
            ShowStyleLine("Respone - " + respone.StatusCode);
            Console.WriteLine(respone.Content.ReadAsStringAsync().Result);
            
        }

        private void RemoveVehicle()
        {
            Console.Write("Please enter id vehicle which you want to delete: ");
            string idVehicle = Console.ReadLine();
            
            var respone = client.DeleteAsync("/api/vehicles/" + idVehicle).Result;
            ShowStyleLine("Respone - " + respone.StatusCode );
            
            if (respone.IsSuccessStatusCode)
            {
                Console.WriteLine("Success!");
                Console.WriteLine(respone.Content.ReadAsStringAsync().Result);
            }
            
        }
        
        private void ReplenishTheBalance()
        {
            ShowStyleLine("Top up balance");
            
            TransactionService transactionService = new TransactionService();

            Console.Write("Enter a id vehicle which you want to top up the balance: ");
            transactionService.Id = Console.ReadLine();
            
            decimal balance;
            do
            {
                Console.Write("Enter sum of money: ");
                
            } while (!decimal.TryParse(Console.ReadLine(), out balance));

            transactionService.Sum = balance;

            string strSerialize = JsonSerializer.Serialize<TransactionService>(transactionService);
            HttpContent httpContent = new StringContent(strSerialize, Encoding.UTF8, "application/json");
            
            var respone = client.PutAsync("/api/transactions/topUpVehicle", httpContent).Result;
            
            ShowStyleLine("Respone - " + respone.StatusCode);
            Console.WriteLine(respone.Content.ReadAsStringAsync().Result);
        }
        
        private void ExitTheProgram()
        {
            Exit = true;
            client.Dispose();
        }
        
        
    }
}