﻿using System;

namespace CoolParkingClient
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                HttpClientMenu menu = new HttpClientMenu();
                menu.StartMenu();
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}